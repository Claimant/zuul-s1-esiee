/**
 * This class use is to get the words returned by the parser and create a
 * Command that correspond the player's words *
 */

public class Command {

	// Attributes

	private String aCommandWord;
	private String aSecondWord;

	// Constructors

	public Command() {
		this.aCommandWord = null;
		this.aSecondWord = null;
	}

	public Command(final String pCommandWord, final String pSecondWord) {
		this();
		this.aCommandWord = pCommandWord;
		this.aSecondWord = pSecondWord;
	}

	// Methods

	public boolean isUnknown() {
		return (this.aCommandWord == null);
	}

	public boolean hasSecondWord() {
		return !(this.aSecondWord == null);
	}

	// Getters & Setters

	public String getCommandWord() {
		return this.aCommandWord;
	}

	public void setCommandWord(final String pCommandWord) {
		this.aCommandWord = pCommandWord;
	}

	public String getSecondWord() {
		return this.aSecondWord;
	}

	public void setSecondWord(final String pSecondWord) {
		this.aSecondWord = pSecondWord;
	}

	@Override
	public String toString() {
		return (this.aCommandWord + " " + this.aSecondWord);
	}

}
