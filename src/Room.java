import java.util.HashMap;

/**
 * A Room is a place where the player will do actions.
 */

public class Room {

    /**
     * Differentes directions
     */

    public static enum eDirections {
        NORTH, EAST, SOUTH, WEST, ABOVE, UNDER
    };

    /**
     * Description of the room and the four possible exits (cardinal points)
     */

    private String aDescription;

    private HashMap<eDirections, Room> aExits;

    public Room() {
        this.aExits = new HashMap<>();
        this.aExits.put(eDirections.NORTH, null);
        this.aExits.put(eDirections.EAST, null);
        this.aExits.put(eDirections.SOUTH, null);
        this.aExits.put(eDirections.WEST, null);
        this.aExits.put(eDirections.ABOVE, null);
        this.aExits.put(eDirections.UNDER, null);
    }

    /**
     * Create a new room with the four exits at null and a description
     * 
     * @param pDescription description of the room
     */

    public Room(final String pDescription) {
        this();
        this.aDescription = pDescription;
    }

    public void setDescription(final String pDescription) {
        this.aDescription = pDescription;
    }

    /**
     * Set all the exits
     * 
     * @param pNRoom
     * @param pERoom
     * @param pSRoom
     * @param pWRoom
     * @param pARoom
     * @param pURoom
     */

    public void setExits(final Room pNRoom, final Room pERoom, final Room pSRoom, final Room pWRoom, final Room pARoom,
            final Room pURoom) {
        this.aExits.put(eDirections.NORTH, pNRoom);
        this.aExits.put(eDirections.EAST, pERoom);
        this.aExits.put(eDirections.SOUTH, pSRoom);
        this.aExits.put(eDirections.WEST, pWRoom);
        this.aExits.put(eDirections.ABOVE, pARoom);
        this.aExits.put(eDirections.UNDER, pURoom);
    }

    /**
     * 
     * @param pDir  Direction of the Room
     * @param pRoom Room to add
     */

    public void setExit(final eDirections pDir, final Room pRoom) {
        this.aExits.put(pDir, pRoom);
    }

    public Room getExit(final eDirections pDir) {
        return this.aExits.get(pDir);
    }

    /**
     * @return the exit it they exist
     */

    public String getExitString() {
        String vToReturn = "";
        for (eDirections vDir : eDirections.values()) {
            Room vRoom = this.getExit(vDir);
            if (vRoom != null) {
                vToReturn += vDir + " : " + vRoom.aDescription + '\n';
            }
        }
        return vToReturn;
    }

    /**
     * Return a string describing the room as : description : exits
     */

    @Override
    public String toString() {
        String vToReturn = this.aDescription + " : \n" + getExitString();
        return vToReturn;
    }

    /**
     * Return a string containing a detailled description of the room
     * 
     * @return
     */

    public String getLongDescription() {
        String vToReturn = (this.toString());
        // TODO ADD MORE INFOS 
        return vToReturn;
    }

}
