public class Calc {

    public static void main(String[] args) {
        Calc c = new Calc();
    }

    public int racNeg(final int pParam) {
        if (pParam >= 0) {
            return (int) (Math.sqrt(pParam));
        } else {
            return (int) (-Math.sqrt(-pParam));
        }
    }

    public void afficheMoities(final int pParam) {
        int vCopieParam = pParam;
        while (vCopieParam != 0) {
            System.out.print("" + vCopieParam + " / ");
            vCopieParam = (int) (vCopieParam / 2);
        }
        System.out.print('1');
    }

}
