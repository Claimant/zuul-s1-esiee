/**
 * Main class of the project that represent the game itself
 */

public class Game {
    // Attributs

    /**
     * Current room
     */

    private Room aCurrentRoom;
    private Parser aParser;
    private CommandWords aCommandWords;

    // Constructors

    /**
     * Create a new instance of Game that will call createRooms() and instanciate
     * all attributes then start the game
     */

    public Game() {
        createRooms();
        aParser = new Parser();
        aCommandWords = new CommandWords();
        this.play();
    }

    // Methods

    /**
     * Create the game map and set the initial room
     */

    private void createRooms() {
        Room vR1 = new Room("Scene de crime 1");
        Room vR2 = new Room("Ruelle adjacente à la scene de crime");
        Room vR3 = new Room("Croisement avec la rue principale");
        Room vR4 = new Room("Coté de la rue opposé à la rue principale");
        Room vR5 = new Room("Entrée des égouts");

        vR1.setExits(vR2, vR3, null, vR4, null, vR5);
        vR2.setExits(null, vR4, vR1, null, null, null);
        vR3.setExits(null, null, null, vR1, null, null);
        vR4.setExits(vR2, vR1, null, null, null, null);
        vR5.setExits(null, null, null, null, vR1, null);

        this.aCurrentRoom = vR1;
    }

    /**
     * Play a turn while the game isn't finished
     */

    public void play() {
        printStart();
        printHelp();
        boolean vFinish = false;
        while (!vFinish) {
            Command vCommand = aParser.getCommand();
            vFinish = processCommand(vCommand);
        }
        System.out.println("Thanks for playing.\nGood bye.");
    }

    /**
     * Process the command given and call the appropriate method
     * 
     * @param pCommand the command that the user enter
     * @return whether the command was used
     */

    private boolean processCommand(final Command pCommand) {
        if (aCommandWords.isCommand(pCommand.getCommandWord())) {
            if (pCommand.getCommandWord().equals("quit")) {
                return (quit(pCommand));
            } else if (pCommand.getCommandWord().equals("go")) {
                goRoom(pCommand);
            } else if (pCommand.getCommandWord().equals("help")) {
                printHelp();
            } else if (pCommand.getCommandWord().equals("current")) {
                printLocationInfo();
            } else if (pCommand.getCommandWord().equals("look")) {
                look(pCommand);
            }
            return false;
        }
        System.out.println("I don't know what do you mean ...");
        return false;
    }

    /**
     * Look in the direction given by the second word of the command if it exists
     * 
     * @param pCommand Given command
     */

    private void look(Command pCommand) {
        if (pCommand.hasSecondWord()) {
            System.out.println("I don't know how to do that yet");
        } else {
            System.out.println("Where should i look ?");
        }
    }

    /**
     * go to the given direction if possible
     * 
     * @param pCommand
     */

    private void goRoom(Command pCommand) {
        if (!pCommand.hasSecondWord()) {
            System.out.println("Go where ?");
        } else {
            String vSecondWord = pCommand.getSecondWord();
            Room vDirection = null;
            if (vSecondWord.equals("north")) {
                vDirection = this.aCurrentRoom.getExit(Room.eDirections.NORTH);
            } else if (vSecondWord.equals("east")) {
                vDirection = this.aCurrentRoom.getExit(Room.eDirections.EAST);
            } else if (vSecondWord.equals("south")) {
                vDirection = this.aCurrentRoom.getExit(Room.eDirections.SOUTH);
            } else if (vSecondWord.equals("west")) {
                vDirection = this.aCurrentRoom.getExit(Room.eDirections.WEST);
            } else {
                System.out.println("I don't know this direction");
            }
            if (vDirection == null) {
                System.out.println("Theres's no door this way");
            } else {
                this.aCurrentRoom = vDirection;
                this.printLocationInfo();
            }
        }
    }

    /**
     * Print the help message
     */

    public void printHelp() {
        System.out.println(
                "You're lost. You're alone.\nYou wander around at the university.\nYour command words are :\n ");
        aCommandWords.printCommands();
    }

    /**
     * Quit the game
     * 
     * @param pCommand command given
     * @return whether the command was correct
     */

    public boolean quit(final Command pCommand) {
        if (!pCommand.hasSecondWord()) {
            System.out.println("Quit what ?");
            return false;
        }
        return true;
    }

    /**
     * Affiche les informations de debut de jeu
     */
    public void printStart() {
        System.out.println("Welcome to the World of Zuul!\n"
                + "World of Zuul is a new, incredibly boring adventure game.\n" + "Type \'help\' if you need help.\n");
        printLocationInfo();
    }

    /**
     * Affiche la localisation actuelle du joueur
     */
    public void printLocationInfo() {
        System.out.println("You're currently in the " + this.aCurrentRoom.toString());
    }

}
