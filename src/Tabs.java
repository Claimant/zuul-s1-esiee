public class Tabs {

    public void afficheValeur(final int[] pTab) {
        for (int i = 0; i < pTab.length; i++) {
            System.out.println("tab[" + i + "] = " + pTab[i]);
        }
    }

    public void afficheValeurInv(final int[] pTab) {
        for (int i = pTab.length - 1; i != 0; i--) {
            System.out.println("tab[" + i + "] = " + pTab[i]);
        }
    }

    public void initTab(final int[] pTab) {
        for (int i = 0; i < pTab.length; i++) {
            pTab[i] = 2 * i;
        }
    }

    public int somme(final int[] pTab) {
        int vSum = 0;
        for (int i : pTab) {
            vSum += i;
        }
        return vSum;
    }

    public int minValue(final int[] pTab) {
        if (pTab.length == 0) {
            return 0;
        }
        int minValue = pTab[0];
        for (int i = 1; i < pTab.length; i++) {
            minValue = minValue > pTab[i] ? pTab[i] : minValue;
        }
        return minValue;
    }

    public int minIndice(final int[] pTab) {
        if (pTab.length == 0) {
            return 0;
        }
        int minValue = 0;
        for (int i = 1; i < pTab.length; i++) {
            minValue = minValue > pTab[i] ? i : minValue;
        }
        return minValue;
    }

    public int maxIndice(final int[] pTab) {
        if (pTab.length == 0) {
            return 0;
        }
        int maxValue = 0;
        for (int i = 1; i < pTab.length; i++) {
            maxValue = maxValue < pTab[i] ? i : maxValue;
        }
        return maxValue;
    }

}
